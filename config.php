<?php

use Illuminate\Support\Str;

return [
    'baseUrl' => 'https://laravelreactspa.santospierre.com/docs/',
    'production' => false,
    'siteName' => 'Laravel React SPA',
    'siteDescription' => 'Authentification Template with Laravel as back-end and React SPA as front-end.',

    // Algolia DocSearch credentials
    'algoliaAppId' => 'G653JXONPB',
    'docsearchApiKey' => 'ead98f7c41c2baffebc7a11ccb279c9c',
    'docsearchIndexName' => 'laravelreactspa',

    // navigation menu
    'navigation' => require_once('navigation.php'),

    // helpers
    'isActive' => function ($page, $path) {
        return Str::endsWith(trimPath($page->getPath()), trimPath($path));
    },
    'isActiveParent' => function ($page, $menuItem) {
        if (is_object($menuItem) && $menuItem->children) {
            return $menuItem->children->contains(function ($child) use ($page) {
                return trimPath($page->getPath()) == trimPath($child);
            });
        }
    },
    'url' => function ($page, $path) {
        return Str::startsWith($path, 'http') ? $path : '/' . trimPath($path);
    },
];
