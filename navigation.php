<?php

return [
    'Getting Started' => [
        'url' => 'docs/getting-started',
    ],
    'Laravel' => [
        'url' => 'docs/laravel/installation',
        'children' => [
            'Fortify' => [
                'url' => 'docs/laravel/fortify/installation'
            ],
            'Sanctum' => [
                'url' => 'docs/laravel/sanctum/installation'
            ],
            'Socialite' => [
                'url' => 'docs/laravel/socialite/installation'
            ]
        ],
    ],
    'React SPA' => [
        'url' => 'docs/react/installation',
        'children' => [
            'Axios' => 'docs/react/axios',
            'Redux' => 'docs/react/redux',
            'React Router' => 'docs/react/react-router',
            'UI' => 'docs/react/ui',
        ],
    ],
];
