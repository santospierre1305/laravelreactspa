window.docsearch = require("docsearch.js");

import Prism from "prismjs";
import "prismjs/components/prism-markup-templating";
import "prismjs/components/prism-php";
import "prismjs/components/prism-bash";
import "prismjs/components/prism-jsx";

Prism.highlightAll();
