---
title: Fortify Installation
description: How to install Fortify to use it as an API authentification system.
extends: _layouts.documentation
section: content
---
# Fortify {#fortify}

## Fortify Installation {#fortify-installation}

Composer package installation

```bash
composer require laravel/fortify
```

Publish the configuration files

```bash
php artisan vendor:publish --provider="Laravel\Fortify\FortifyServiceProvider"
```
> The command will publish files that are used by fortify in the  `app/Actions` directory.

Add the `FortifyServiceProvider` to our providers in `config/app.php`.

``` php
// config/app.php
return [
    /* ... */
    'providers' => [
        /* ... */
        /*
        * Application Service Providers...
        */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,
        App\Providers\FortifyServiceProvider::class
        /* ... */
    ],
    /* ... */
]
```

Migrate the database

```bash
php artisan migrate
```

## Fortify Configuration {#fortify-configuration}

### Environment Variable {#environment-variable}

```php
SPA_URL='http://localhost:3000' // React SPA Url
```

### Fortify Features {#fortify-features}

>All the changes in this section will be made in the config file `config/fortify.php`.

After the user get logged in or reset his password he will be redirected to this url.

```php
//config/fortify.php
return [
    /*... */
    'home' => env('SPA_URL'),
    /*... */
]
```

As we use React we are going to tell Fortify that we dont want to use the default views.

```php
//config/fortify.php
return [
    /*... */
    'views' => false,
    /*... */
]
```

The features that we want to use from Fortify, for example if you don't want to implement the email verification you can just comment it, and the routes will not be create for this feature.


```php
//config/fortify.php
return [
    /* ... */
    'features' => [
        Features::registration(),
        Features::emailVerification(),
        Features::resetPasswords(),
        Features::updateProfileInformation(),
        Features::updatePasswords(),
    ],
    /* ... */
]
```

### CORS Setup {#cors-setup}

The routes provide by Fortify use the `web` middleware, to avoid CORS issues we must add those routes to the whitelist, in the `config/cors.php` file.
```php
//config/cors.php
return [
    /*... */
    'paths' => [
        /*...*/
        'login',
        'logout',
        'register',
        'user/password',
        'forgot-password',
        'reset-password',
        'user/profile-information',
        'email/verification-notification',
    ],
    /*... */
    'supports_credentials' => true,
]
```

To allow the credentials (Cookies, Authorization Header, ...) in the front-end we must also set the `supports_crendentials` at **`true`**.

### Reset Password {#reset-password}

If you want reset password feature you need to overwrite the route created with Fortify by a custom one corresponding to your React SPA url, in your boot method in the `app/Providers/AuthServiceProvider.php` file.

```php
// app/Providers/AuthServiceProvider.php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        ResetPassword::createUrlUsing(function ($user, string $token) {
            return env('SPA_URL') . '/reset-password?token=' . $token;
        });
    }
}

```
