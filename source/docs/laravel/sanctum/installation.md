---
title: Sanctum Installation
description: How to install Sanctum to authenticate an SPA.
extends: _layouts.documentation
section: content
---
# Sanctum {#sanctum}

## Sanctum Instalation {#sanctum-installation}
Composer package installation

```bash
composer require laravel/sanctum
```

Publish the configuration files

```bash
php artisan vendor:publish --provider="Laravel\Sanctum\SanctumServiceProvider"
```
> The command will publish files that are used by fortify in the  `app/Actions` directory.

Migrate the database

```bash
php artisan migrate
```

## Sanctum Configuration {#sanctum-configuration}

Sanctum can authenticate your application with tokens, but for simplicity and mostly because I don't want to manage token in the client side, Sanctum provide an easy way to login your SPA.


### Environment Variable {#environment-variable}

```php
SANCTUM_STATEFUL_DOMAINS='localhost:3000' // React SPA URL
SESSION_DOMAIN='localhost' // Root Domain
```

> To use Sanctum SPA your front and back need to be in the same root-domain.

### Middleware {#middleware}

For SPA Authentification we need to add the Sanctum middleware in our api routes, by modifying the `app/Http/Kernel.php` file.

```php
//app/Http/Kernel.php
'api' => [
    \Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful::class,
    'throttle:api',
    \Illuminate\Routing\Middleware\SubstituteBindings::class,
],
```
### Retrieve Current User {#retreive-current-user}

Example for getting an the autenthicated user, we need to add the `auth:sanctum` middleware to the route in our `routes/api.php` file.

```php
//routes/api.php
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return new UserResource($request->user());
});
```

>I use a `UserResource` to retrieve information like I want, that way I don't manipulate them in the fron-end. Highly recommended method for Laravel API <a href="https://laravel.com/docs/8.x/eloquent-resources" target="_blank">More Info</a>

The code for the *UserResource*

```php
<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public static $wrap = ''; // Remove the data wrapper
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'is_verified' => !is_null($this->email_verified_at), // Check if the user has a verified address
            // Hide if not using github login
            'is_github_account' => !is_null($this->provider_id) // Check if the user is signed with Github
        ];
    }
}
```

### CORS Setup {#cors-setup}

We need to add a new route to our whitelist, this route will be call when we need to synchronize the cookies with Laravel and the SPA.

```php
//config/cors.php
return [
    /*... */
    'paths' => [
        /*...*/
        'sanctum/csrf-cookie',
        /*...*/
    ],
    /*... */
]
```
