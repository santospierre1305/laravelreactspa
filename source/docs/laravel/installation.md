---
title: Laravel
description: Requirement packages for laravel
extends: _layouts.documentation
section: content
---
# Laravel {#laravel}

## Auth Packages {#auth-package}

The packages to make the authentication simpler thanks to Laravel :

+ <a href="https://laravel.com/docs/8.x/fortify" target="_blanck">Fortify</a>
+ <a href="https://laravel.com/docs/8.x/sanctum" target="_blanck">Sanctum</a>
+ <a href="https://laravel.com/docs/8.x/socialite" target="_blanck">Socialite</a>

## Sail {#sail}

If you use Docker or Docker Desktop on Windows(WSL2)/Mac, I highly recommend to work with <a href="https://laravel.com/docs/8.x/sail" target="_blanck">Sail</a>, it offers you a nice bundle to work in development easily without issuing with all the dependencies. Go check the <a href="https://laravel.com/docs/8.x/sail" target="_blanck">documentation</a> and try it!

>Can be implemented in a current project or brand new!

## Github Repository {#github-repository}

If you want to check the [repository](https://github.com/santos-pierre/react-spa-auth).
