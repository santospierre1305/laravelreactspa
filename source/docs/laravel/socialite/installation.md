---
title: Socialite Installation
description: Setup Socialite Laravel
extends: _layouts.documentation
section: content
---
# Socialite {#socialite}

## Socailite Installation {#socialite-installation}

Composer package installation

```bash
composer require laravel/socialite
```

## Github {#github}

### Create OAuth App {#create-oauth-app}

There is the officially doc to <a href="https://docs.github.com/en/developers/apps/creating-an-oauth-app" target="_blanck">create an OAuth App</a> and below my configuration as an example.

<img src="/assets/images/docs/oauth_app.PNG" class="mx-auto rounded-md">


### Configuration {#configuration}

After creating your app you will get `CLIENT_ID` and ` CLIENT_SECRET` add them in your environment variable

```php
// .env
GITHUB_CLIENT_ID=/*CLIENT_ID*/
GITHUB_CLIENT_SECRET=/*CLIENT_SECRET*/
GITHUB_CALLBACK=/*CALLBACK URL*/
```

Refer those variables in `config/services.php`.

```php
// config/services.php
return [
    /*...*/
    'github' => [
        'client_id' => env('GITHUB_CLIENT_ID'),
        'client_secret' => env('GITHUB_CLIENT_SECRET'),
        'redirect' => env('GITHUB_CALLBACK'),
    ],
]
```
### Model and Table {#model-and-table}

When we sign in with Github, the password field is empty, so we need to set our password to `nullable()` to avoid an error when creating the new user. We will also store a `provider_id` that correspond at the user_id from Github.

``` php
// database/migrations/2014_10_12_000000_create_users_table.php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('provider_id')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
```

In the User model add the `provider_id` to fillable.

``` php
//app/Models/User.php
namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email_verified_at',
        'name',
        'email',
        'password',
        'provider_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}

```


### Controller and Routes {#controller-and-routes}

Thanks to socialite we can reach github to give us the informations for a given user. We gonna create one route to redirect to the authentication page of github and an other to manage the login of the given user.

In the `routes.php`
```php
Route::get('github/auth/login', [LoginController::class, 'redirectToProvider']);

Route::get('github/auth/callback', [LoginController::class,'handleProviderCallback']);
```

Create a new controller `app/Http/Controllers/LoginController.php`

```php
//app/Http/Controllers/LoginController.php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    public function redirectToProvider()
    {
        // Redirect the user to the Github Login Screen
        return Socialite::driver('github')->redirect();
    }

    public function handleProviderCallback()
    {
        // Get the user from Github
        $user = Socialite::driver('github')->user();

        // Create the user if he doesn't exist
        $newUser = User::firstOrCreate(
            [
                'email' => $user->getEmail(),
                'provider_id' => $user->getId()
            ],
            [
                'name' => $user->getName(),
                'email_verified_at' => now()
            ]
        );
        // Log the user
        Auth::login($newUser);
        // After finishing the login redirect to our SPA
        return redirect(env('SPA_URL'));
    }
}
```
### CORS Setup {#cors-setup}

We need to add new routes to our whitelist, because the methods above need the laravel session to work.
```php
//config/cors.php
return [
    /*... */
    'paths' => [
        /*...*/
        'github/auth/login',
        'github/auth/callback'
        /*...*/
    ],
    /*... */
]
```
