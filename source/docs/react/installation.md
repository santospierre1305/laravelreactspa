---
title: React SPA
description: Requirement packages for REACT SPA
extends: _layouts.documentation
section: content
---
# React SPA {#react-spa}

## React Packages {#react-package}

The packages I used to build the SPA:

+ <a href="https://github.com/axios/axios" target="_blanck">Axios</a>
+ <a href="https://redux.js.org/" target="_blanck">Redux & React-Redux</a>
+ <a href="https://github.com/reduxjs/redux-thunk" target="_blanck">Redux Thunk</a> for asynchronous call with Redux.
+ <a href="https://tailwindcss.com" target="_blanck">Tailwind CSS</a> you can check the <a href="https://tailwindcss.com/docs/guides/create-react-app" target="_blanck">official documentation </a> to install Tailwind CSS with React.
+ <a href="https://headlessui.dev/" target="_blanck">Transition from HeadlessUI</a> to manage transition with the Tailwind CSS classes.

## Github Repository {#github-repository}

If you want to check the [repository](https://github.com/santos-pierre/react-spa-auth).

## React Template {#react-template}

You can create a new project with the template.

``` bash
npx create-react-app my-app --template laravel-auth
```
