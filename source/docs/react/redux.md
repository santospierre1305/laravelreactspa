---
title: React Redux SPA
description: React Redux SPA
extends: _layouts.documentation
section: content
---

# Redux {#redux}

## Redux User State {#redux-user-state}
> I assume that you have basic knowledge of redux, I tried to make it the most simple and easy to read.

For manage the authentification and be sure to access it everywhere in the app, We store it in our redux store.

### User Types/Actions {#user-types-actions}

Types are all the kind of action we have for our user. Here we simply need to set our state with the current user.
``` javascript
//src/redux/users/userTypes.js
export const userTypes = {
    SET_USER: 'SET_USER',
};
```

Next, we defined the actions that correspond to the type we defined above, with the payload, the content of our futur state.

``` javascript
//src/redux/users/userActions.js
import { userTypes } from './userTypes';
import usersClient from './../../api/users/usersClient';

export const setCurrentUser = (user) => {
    return {
        type: userTypes.SET_USER,
        payload: user,
    };
};

export const getAuthUser = () => async (dispatch) => {
    try {
        const response = await usersClient.getAuthUser();
        dispatch(setCurrentUser(response.data));
        return Promise.resolve(response);
    } catch (errors) {
        dispatch(setCurrentUser(null));
        return Promise.reject(errors);
    }
};

export const logout = () => async (dispatch) => {
    try {
        const response = await usersClient.logout();
        dispatch(setCurrentUser(null));
        return Promise.resolve(response);
    } catch (errors) {
        dispatch(setErrors(errors));
        return Promise.resolve(errors);
    }
};
```

> `getAuthUser()` and `logout()` are async redux store function, to be able to reach our server before dispatching our store . Redux cannot handle asynchronous call so we need to install `redux-thunk` middleware to help us. We are going to setup the middleware in our store configuration.

### User Reducer/Selectors {#user-reducer-selector}

The Reducer where all the magic happens, with our action and our payload, we can now setup the logic of our actions. Here we just replace our current state with a new one.
```javascript
// src/redux/users/userReducer.js
import { userTypes } from './userTypes';

const INITIAL_STATE = null;

const userReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case userTypes.SET_USER:
            return { ...state, ... action.payload };
        default:
            return state;
    }
};

export default userReducer;
```

Selectors are functions to get a slice of our store.

```javascript
// src/redux/users/userSelector.js
export const getUser = (state) => state.user;
```

## Redux Notification State {#redux-notification-state}

Notifications system to show to the user if everything went well when he update his profile, send verfication mail, ...

> The code is pratically the same for the user state

### Notification Types/Actions {#notification-types-actions}
``` javascript
//src/redux/notifications/notificationTypes.js
export const notificationTypes = {
    SET_NOTIFICATION: 'SET_NOTIFICATION',
};
```

``` javascript
//src/redux/notifications/notificationActions.js
import { notificationTypes } from './notificationTypes';

export const setNotification = (message) => {
    return {
        type: notificationTypes.SET_NOTIFICATION,
        payload: message,
    };
};
```

### Notification Reducer/Selectors {#notification-reducer-selectors}
```javascript
// src/redux/notifications/notificationReducer.js
import { notificationTypes } from './notificationTypes';

const INITIAL_STATE = {
    message: '',
    status: '',
    show: false,
};

const notificationReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case notificationTypes.SET_NOTIFICATION:
            return {
                ...state,
                message: action.payload.message,
                status: action.payload.status,
                show: action.payload.show,
            };
        default:
            return state;
    }
};

export default notificationReducer;
```

```javascript
// src/redux/notifications/notificationSelector.js
export const getNotification = (state) => state.notification;
```

## Redux Main Reducer {#redux-main-reducer}

At the moment we have two pieces of reducers with their own state, we need to bring them together with the `combineReducers()` functions from redux to access them entirely in our App.

```javascript
import { combineReducers } from 'redux';
import notificationReducer from './notifications/notificationReducer';
import userReducer from './users/userReducer';

const mainReducer = combineReducers({
    user: userReducer,
    notification: notificationReducer,
});

export default mainReducer;
```

## Redux Store Setup {#reduc-store-setup}

Now we create our store with the `mainreducer` created earlier with the function `createStore()` from redux. We had our reducer and the options we need `redux-thunk` and the `redux-devtools-extension`.
``` javascript
//src/redux/store.js
import mainReducer from './mainReducer';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

export const store = createStore(
    mainReducer,
    composeWithDevTools(applyMiddleware(thunk))
);

```
> Highly Recommend to install `redux-devtools-extension` when using redux is a time saver!
