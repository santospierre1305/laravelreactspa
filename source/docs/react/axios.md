---
title: Axios - React SPA
description: Axios Setup API CLient
extends: _layouts.documentation
section: content
---

# Axios {#axios}

## Axios Client Creation {#axios-client-creation}

To avoid to import axios everywhere, we create a client with the basic setup for axios.

```javascript
// src/api/client/client.js
import axios from 'axios';

const getClient = (baseURL) => {
    const options = {
        baseURL: baseURL,
        withCredentials: true, // IMPORTANT TO BE SET AT TRUE
        headers: {
            Accept: 'application/json', // TO BE SURE TO HAVE ALWAYS JSON RESPONSE
        },
    };

    let client = axios.create(options);

    const handleResponse = (response) => {
        return Promise.resolve(response);
    };

    const handleError = (error) => {
        if (error.response.status === 422) {
            return Promise.reject({
                status: error.response.status,
                errors: error.response.data.errors,
            });
        } else {
            return Promise.reject({
                status: error.response.status,
                message: error.response.data.message,
            });
        }
    };

    client.interceptors.response.use(handleResponse, handleError);

    return client;
};

export { getClient };

```
> `handleError` method is called every time the request hits an error then format the error message to correspond to what I need.


## API Creation {#api-creation}

Now with the client setup we gonna add all the Request Methods (`GET`, `POST`, `PATCH`, ...) to it, if in the future we want to make a change to all `GET` methods we will modify it here.

```javascript
//src/api/client/apiClient.js
import { getClient } from './client';

class ApiClient {
    constructor(baseUrl = null) {
        this.client = getClient(baseUrl);
    }

    get(url, conf = {}) {
        return this.client
            .get(url, conf)
            .then((response) => Promise.resolve(response))
            .catch((error) => Promise.reject(error));
    }

    delete(url, conf = {}) {
        return this.client
            .delete(url, conf)
            .then((response) => Promise.resolve(response))
            .catch((error) => Promise.reject(error));
    }

    head(url, conf = {}) {
        return this.client
            .head(url, conf)
            .then((response) => Promise.resolve(response))
            .catch((error) => Promise.reject(error));
    }

    options(url, conf = {}) {
        return this.client
            .options(url, conf)
            .then((response) => Promise.resolve(response))
            .catch((error) => Promise.reject(error));
    }

    post(url, data = {}, conf = {}) {
        return this.client
            .post(url, data, conf)
            .then((response) => Promise.resolve(response))
            .catch((error) => Promise.reject(error));
    }

    put(url, data = {}, conf = {}) {
        return this.client
            .put(url, data, conf)
            .then((response) => Promise.resolve(response))
            .catch((error) => Promise.reject(error));
    }

    patch(url, data = {}, conf = {}) {
        return this.client
            .patch(url, data, conf)
            .then((response) => Promise.resolve(response))
            .catch((error) => Promise.reject(error));
    }
}

export { ApiClient };

```

## User Client {#user-client}

Here is the real stuff, the user client, the methods, depending on your Fortify features, that we gonna to use in our code.

```javascript
//src/api/client/users/userClient.js
import { ApiClient } from './../client/apiClient';

// Check out https://create-react-app.dev/docs/adding-custom-environment-variables/
let client = new ApiClient(process.env.REACT_APP_API_URL);

const userClient = {
    async login(payload) {
        await client.get('/sanctum/csrf-cookie');
        await client.post('/login', payload);
    },
    async logout() {
        return await client.post('/logout');
    },
    async forgotPassword(payload) {
        await client.get('/sanctum/csrf-cookie');
        await client.post('/forgot-password', payload);
    },
    async getAuthUser() {
        return await client.get('api/user');
    },
    async resetPassword(payload) {
        await client.get('/sanctum/csrf-cookie');
        await client.post('/reset-password', payload);
    },
    async updatePassword(payload) {
        await client.put('/user/password', payload);
    },
    async registerUser(payload) {
        await client.get('/sanctum/csrf-cookie');
        await client.post('/register', payload);
    },
    async sendVerification(payload) {
        await client.post('/email/verification-notification', payload);
    },
    async updateUser(payload) {
        await client.put('/user/profile-information', payload);
    },
};

export default userClient;
```
