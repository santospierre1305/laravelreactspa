---
title: Manage Route SPA
description: Guarded route for React SPA
extends: _layouts.documentation
section: content
---

# React Router {#react-router}

## Links (Single point of truth) {#link-single-point-of-truth}

To avoid to write always the path of our links or routes, we put them in one place and we gonna access them with a `getRoute()` methods. Thanks to that we have a file that group up all our routes, a single point of truth, so in the future if we need to change the path of a component, we don't need to go in each files and change it.

> We use the `name` as an id to get our route path, so never change it and keep it unique.

First the routes that we gonna to use for react-router.

```js
//src/routes/routes.js
const ROUTES = [
    {
        name: 'login',
        path: '/login',
    },
    {
        name: 'register',
        path: '/register',
    },
    {
        name: 'forgot-password',
        path: '/forgot-password',
    },
    {
        name: 'reset-password',
        path: '/reset-password',
    },
    {
        name: 'home',
        path: '/',
    },
    {
        name: 'profile',
        path: '/profile',
    },
];
```
Second the links of my navbar.
```js
//src/routes/routes.js
const NavBarLinks = [
    {
        name: 'Dashboard',
        path: '/',
    },
    {
        name: 'Profile',
        path: '/profile',
    },
];
```
The methods we will use accross the app to access our links.

``` js
//src/routes/routes.js
const getAllRoutes = () => {
    return ROUTES;
};

const getAllNavLinks = () => {
    return NavBarLinks;
};

const getRoute = (name, params) => {
    let route = ROUTES.filter((route) => route.name === name)[0];
    if (params) {
        for (const [key, value] of Object.entries(params)) {
            route = { ...route, path: route.path.replace(`:${key}`, value) };
        }
    }
    return route;
};

export { getAllRoutes, getRoute, getAllNavLinks };
```

## Public Route {#public-route}

If the user is authenticate he cannot access to the login page, so we need a public route, that check everytime we hit a route that the user is authenticated or not.

```jsx
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { getAuthUser } from '../../redux/users/userActions';
import Loading from '../Loading/Loading';

const PublicRoute = ({ component: Component, ...rest }) => {
    const dispatch = useDispatch();
    const [isAuthenticate, setIsAuthenticate] = useState(false);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const auth = async () => {
            try {
                await dispatch(getAuthUser());
                setIsAuthenticate(true);
            } catch (error) {
                setIsAuthenticate(false);
            } finally {
                setIsLoading(false);
            }
        };
        auth();
    }, [dispatch]);

    return !isLoading ? (
        <Route
            {...rest}
            render={(props) =>
                !isAuthenticate ? (
                    <Component {...props} />
                ) : (
                    <Redirect to={'/'} />
                )
            }
        />
    ) : (
        <Loading />
    );
};

export default PublicRoute;

```

## Guarded Route {#guarded-route}

Pretty much the same just invert the boolean and change the redirect path.

```jsx
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { getAuthUser } from '../../redux/users/userActions';
import Loading from '../Loading/Loading';

const GuardedRoute = ({ component: Component, ...rest }) => {
    const dispatch = useDispatch();
    const [isAuthenticate, setIsAuthenticate] = useState(false);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const auth = async () => {
            try {
                await dispatch(getAuthUser());
                setIsAuthenticate(true);
            } catch (error) {
                setIsAuthenticate(false);
            } finally {
                setIsLoading(false);
            }
        };
        auth();
    }, [dispatch]);

    return !isLoading ? (
        <Route
            {...rest}
            render={(props) =>
                isAuthenticate ? (
                    <Component {...props} />
                ) : (
                    <Redirect to={'/login'} />
                )
            }
        />
    ) : (
        <Loading />
    );
};

export default GuardedRoute;
```

## RouterSwitch Component {#routerswitch-component}

Now we group up all the routes to a `Switch` from `react-router-dom`, he will check the url path and render the component linked to it.

We use `getRoute(name)` to retrieve the correct path we defined above earlier.

```jsx
import React from 'react';
import { Redirect, Switch } from 'react-router-dom';
import GuardedRoute from '../components/GuardedRoute/GuardedRoute';
import PublicRoute from '../components/PublicRoute/PublicRoute';
import { getRoute } from './routes';
import Dashboard from '../pages/Dashboard/Dashboard';
import UserProfile from '../pages/Dashboard/UserProfile/UserProfile';
import Login from '../pages/Login/Login';
import Register from '../pages/Register/Register';
import ForgotPassword from '../pages/ForgotPassword/ForgotPassword';
import ResetPassword from '../pages/ResetPassword/ResetPassword';

const RouterSwitch = () => {
    return (
        <Switch>
            <GuardedRoute
                component={Dashboard}
                path={getRoute('home').path}
                exact={true}
            />
            <GuardedRoute
                component={UserProfile}
                path={getRoute('profile').path}
                exact={true}
            />
            <PublicRoute
                component={Login}
                path={getRoute('login').path}
                exact={true}
            />
            <PublicRoute
                component={Register}
                path={getRoute('register').path}
                exact={true}
            />
            <PublicRoute
                component={ForgotPassword}
                path={getRoute('forgot-password').path}
                exact={true}
            />
            <PublicRoute
                component={ResetPassword}
                path={getRoute('reset-password').path}
            />
            <Redirect to={getRoute('home').path} />
        </Switch>
    );
};

export default RouterSwitch;
```


The switch read our routes from top to bottom and stop when he find a match with the url and render the component.

```jsx
// If I hit the url '/dashboard/profile'
// the switch will render the Dashboard Component! The first one
// Because he will match the dashboard word first
// so we use the "exact" props to tell react-route-dom be more strict we the path

<Switch>
    <GuardedRoute
        component={Dashboard}
        path='/dashboard'
    />
    <GuardedRoute
        component={UserProfile}
        path='/dashboard/profile'
    />
</Switch>

```
