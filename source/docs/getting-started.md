---
title: Getting Started
description: Getting started with Laravel React SPA Guide. Quick installation.
extends: _layouts.documentation
section: content
---

## Introduction {#introduction}

This is a guide for creating apps with Laravel and React. Everytime I started a Laravel project and didn't want to use Blade/Livewire or InertiaJs, it was difficult to find out someone using ReactJS because most of Laravel developer use Vue, so I decided to share with you this guide. Hope it will help you ! Let's go!

> <a href="https://laravelreact-spa.santospierre.com/" target="_blanck">Click here to see a demo</a>

## Quick Installation {#quick-installation}

> The Front and the Back must be in the same root domain! Or the authentification will not work.

## Laravel Project {#laravel-project}

Clone the [repository](https://github.com/santos-pierre/laravel-auth-api).

```bash
git clone https://github.com/santos-pierre/laravel-auth-api.git

cd laravel-auth-api
```

Create a new `.env` with the content of `.env.example`.

```bash
cp .env.example .env
```

Replace with your environment variables.

```php
//...
SPA_URL='http://localhost:3000'

SANCTUM_STATEFUL_DOMAINS='localhost:3000'
SESSION_DOMAIN=localhost

GITHUB_CLIENT_ID= /* YOUR CLIENT ID */
GITHUB_CLIENT_SECRET= /* YOUR SECRET CLIENT ID */
GITHUB_CALLBACK='http://localhost/github/auth/callback'

```

> The template use Sail for the project so there is no need to change other environment variable. But if you don't use it change the database connection, mail services corresponding to your needs.

**Install the dependencies and start your project !**

``` bash
composer install

php artisan key:generate
```
Sail User
``` bash
sail up -d --build
```
Others
``` bash
php artisan migrate

php artisan serve
```

> Remove the .git folder and launch git init again to have a nice clean project!

## React Project {#react-project}

You can create a new project with the template

``` bash
npx create-react-app my-app --template laravel-auth
```

Change you environment variables in `.env.development`

```php
REACT_APP_API_URL="http://localhost:80/" // Backend Domain
REACT_APP_GITHUB_REDIRECT="http://localhost/github/auth/login"
```
