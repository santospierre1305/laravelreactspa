@extends('_layouts.master')

@section('body')
<section class="container max-w-6xl max-h-full px-6 py-10 mx-auto md:py-12">
    <div class="flex flex-col-reverse my-20 lg:flex-row lg:mb-24">
        <div class="mt-8">
            <h1 id="intro-docs-template">{{ $page->siteName }}</h1>

            <h2 id="intro-powered-by-jigsaw" class="mt-4 font-light">{{ $page->siteDescription }}</h2>

            <div class="flex my-10">
                <a href="/docs/getting-started" title="{{ $page->siteName }} getting started" class="px-6 py-2 mr-4 font-normal text-white bg-blue-500 rounded hover:bg-blue-600 hover:text-white">Get Started</a>
            </div>
        </div>

        <img src="/assets/img/logo.svg" alt="{{ $page->siteName }} large logo" class="h-64 mx-auto mb-6 lg:mb-0 ">
    </div>
</section>
@endsection
