<?php

return [
    'baseUrl' => 'https://laravelreactspa.santospierre.com/',
    'production' => true,

    // DocSearch credentials
    'algoliaAppId' => 'G653JXONPB',
    'docsearchApiKey' => 'ead98f7c41c2baffebc7a11ccb279c9c',
    'docsearchIndexName' => 'laravelreactspa',
];
